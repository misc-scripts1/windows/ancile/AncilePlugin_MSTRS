# Ancile Microsoft Telemetry Reporting System
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSTRS

Ancile Microsoft Telemetry Reporting System plugin disables Microsoft's core telemetry reporting.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile